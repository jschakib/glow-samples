layout(vertices = 3) out;

uniform float uOuterLvlA = 1.0;
uniform float uOuterLvlB = 1.0;
uniform float uOuterLvlC = 1.0;
uniform float uInnerLvl = 1.0;

void main(void)
{
    gl_TessLevelOuter[0] = uOuterLvlA;
    gl_TessLevelOuter[1] = uOuterLvlB;
    gl_TessLevelOuter[2] = uOuterLvlC;

    gl_TessLevelInner[0] = uInnerLvl;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}