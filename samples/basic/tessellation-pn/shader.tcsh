layout(vertices = 3) out;

uniform float uTessellationLevel = 1.0;

in vec3 vNormal[];
in vec3 vWorldPos[];

out vec3 eNormal[];
out vec3 eWorldPos[];

void main(void)
{
    gl_TessLevelOuter[0] = uTessellationLevel;
    gl_TessLevelOuter[1] = uTessellationLevel;
    gl_TessLevelOuter[2] = uTessellationLevel;

    gl_TessLevelInner[0] = uTessellationLevel;

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    eNormal[gl_InvocationID] = vNormal[gl_InvocationID];
    eWorldPos[gl_InvocationID] = vWorldPos[gl_InvocationID];
}