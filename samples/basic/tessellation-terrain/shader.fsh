#include "terrain.glsl"

uniform vec3 uLightDir;
uniform bool uShowNormals;
uniform bool uShowEdgeLength;
uniform float uHeight;
uniform float uWidth;
uniform mat4 uProj;
uniform mat4 uInvProj;

out vec3 fColor;

in float gEdgeLength;
in vec3 gWorldPos;
in float gWorldEdge;
in vec3 gViewPos;
in vec4 gScreenPos;

vec3 hsv2rgb(vec3 c) 
{
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
    // get normal via finite diff
    
    float eps = max(length(dFdx(gWorldPos)), length(dFdx(gWorldPos)));

    //eps = gWorldEdge / 3;

    vec4 wp1 = uInvProj * (gScreenPos / gScreenPos.w - vec4(0.5 / uWidth, 0.5 / uHeight, 0, 0));
    vec4 wp2 = uInvProj * (gScreenPos / gScreenPos.w + vec4(0.5 / uWidth, 0.5 / uHeight, 0, 0));
    eps = distance(wp1.xyz / wp1.w, wp2.xyz / wp2.w);

    // fColor = hsv2rgb(vec3(eps, 1, 1));
    // return;

    //eps = uHeight * uProj[1][1] *

    float h00 = terrainAt(gWorldPos.x, gWorldPos.z, eps);
    float h10 = terrainAt(gWorldPos.x + eps, gWorldPos.z, eps);
    float h01 = terrainAt(gWorldPos.x, gWorldPos.z + eps, eps);

    vec3 p00 = vec3(gWorldPos.x, h00, gWorldPos.z);
    vec3 p10 = vec3(gWorldPos.x + eps, h10, gWorldPos.z);
    vec3 p01 = vec3(gWorldPos.x, h01, gWorldPos.z + eps);

    vec3 N = normalize(cross(p01 - p00, p10 - p00));

    fColor = vec3(1, 1, 1) * (dot(N, uLightDir) * .5 + .5);
    fColor = vec3(1, 1, 1) * max(0.0, dot(N, uLightDir));

    if (uShowEdgeLength)
    {
        if (gEdgeLength < 10)
            fColor = hsv2rgb(vec3(gEdgeLength / 10, 1, 1));
        else
            fColor = vec3(1,1,1) * 10 / gEdgeLength;
    }

    if (uShowNormals)
    {
        fColor = N;
    }
}