#include "TessellationTerrainSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/data/ColorSpace.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void TessellationTerrainSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");

    auto cam = getCamera();
    cam->setLookAt({12, 12, 12}, {0, 0, 0});
    cam->setNearPlane(0.001f);

    std::vector<glm::vec3> vertices;
    vertices.push_back({0, 0, 0});
    vertices.push_back({1, 0, 0});
    vertices.push_back({1, 0, 1});
    vertices.push_back({0, 0, 1});
    auto ab = ArrayBuffer::create();
    ab->defineAttribute<glm::vec3>("aPosition");
    ab->bind().setData(vertices);
    mQuad = VertexArray::create(ab, GL_PATCHES);
    mQuad->setVerticesPerPatch(4);

    TwAddVarRW(tweakbar(), "Light Dir", TW_TYPE_DIR3F, &mLightDir, "");
    TwAddVarRW(tweakbar(), "Show Wireframe", TW_TYPE_BOOLCPP, &mWireframe, "");
    TwAddVarRW(tweakbar(), "Show Normals", TW_TYPE_BOOLCPP, &mShowNormals, "");
    TwAddVarRW(tweakbar(), "Show Edge Length", TW_TYPE_BOOLCPP, &mShowEdgeLength, "");
    TwAddVarRW(tweakbar(), "Target Edge Length", TW_TYPE_FLOAT, &mEdgeLength, "step=1.0 min=1.0 max=128.0");
    TwAddVarRW(tweakbar(), "Quads per Dim", TW_TYPE_INT32, &mQuads, "min=1 max=1024");
    TwAddVarRW(tweakbar(), "Quad Size", TW_TYPE_FLOAT, &mQuadSize, "step=0.1 min=0.1 max=128.0");

    TwAddVarRW(tweakbar(), "Amplitude", TW_TYPE_FLOAT, &mAmplitude, "step=0.1 min=0.1 max=64.0 group=terrain");
    TwAddVarRW(tweakbar(), "Wavelength", TW_TYPE_FLOAT, &mWavelength, "step=0.1 min=0.1 max=64.0 group=terrain");
    TwAddVarRW(tweakbar(), "Amplitude Factor", TW_TYPE_FLOAT, &mAmplitudeFactor, "step=0.01 min=0.1 max=0.9 group=terrain");
    TwAddVarRW(tweakbar(), "Wavelength Factor", TW_TYPE_FLOAT, &mWavelengthFactor, "step=0.01 min=0.1 max=0.9 group=terrain");
    TwAddVarRW(tweakbar(), "Max Iterations", TW_TYPE_INT32, &mMaxLevels, "min=1 max=64 group=terrain");
}

void TessellationTerrainSample::render(float elapsedSeconds)
{
    GLOW_SCOPED(clearColor, 0.00, 0.33, 0.66, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_CULL_FACE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto cam = getCamera();

    auto shader = mShader->use();
    shader.setUniform("uAmplitude", mAmplitude);
    shader.setUniform("uWavelength", mWavelength);
    shader.setUniform("uAmplitudeFactor", mAmplitudeFactor);
    shader.setUniform("uWavelengthFactor", mWavelengthFactor);
    shader.setUniform("uMaxLevels", mMaxLevels);

    shader.setUniform("uLightDir", mLightDir);
    shader.setUniform("uQuads", mQuads);
    shader.setUniform("uQuadSize", mQuadSize);
    shader.setUniform("uShowNormals", mShowNormals);
    shader.setUniform("uShowEdgeLength", mShowEdgeLength);
    shader.setUniform("uEdgeLength", mEdgeLength);
    shader.setUniform("uWidth", (float)getWindowWidth());
    shader.setUniform("uHeight", (float)getWindowHeight());
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uInvView", inverse(cam->getViewMatrix()));
    shader.setUniform("uInvProj", inverse(cam->getProjectionMatrix()));
    shader.setUniform("uViewProj", cam->getProjectionMatrix() * cam->getViewMatrix());

    GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

    mQuad->bind().draw(mQuads * mQuads);
}
