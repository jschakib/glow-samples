#include "TextureParametersSample.hh"

#include <glm/ext.hpp>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void TextureParametersSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mCube = geometry::Cube<>().generate();

    mTextures.push_back(Texture2D::createFromFile(util::pathOf(__FILE__) + "/img-test.png", ColorSpace::sRGB));

    {
        auto tex = Texture2D::create(512, 512, GL_RGB);
        auto t = tex->bind();
        auto s = 512;
        auto i = 0;
        while (s >= 1)
        {
            t.setData(GL_RGB, s, s, std::vector<glm::vec3>(s * s, rgbColor(glm::vec3(i * 60.0f, 1.0f, 1.0f))), i);
            s /= 2;
            ++i;
        }
        tex->setMipmapsGenerated(true);
		mTextures.push_back(tex);
    }

    auto cam = getCamera();
    cam->setLookAt({2, 2, 2}, {0, 0, 0});
    cam->setNearPlane(0.001f);

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");
    TwAddVarRW(tweakbar(), "Texture #", TW_TYPE_INT32, &mCurrentTexture, "");
    TwAddVarRW(tweakbar(), "Anisotropic Filtering", TW_TYPE_FLOAT, &mAnsitropicFiltering, "step=0.1 min=1.0 max=16.0");
    TwAddVarRW(tweakbar(), "LOD Bias", TW_TYPE_FLOAT, &mLODBias, "step=0.1 min=-10.0 max=10.0");
    TwAddVarRW(tweakbar(), "LOD Min", TW_TYPE_FLOAT, &mLODMin, "step=0.1 min=-10.0 max=10.0");
    TwAddVarRW(tweakbar(), "LOD Max", TW_TYPE_FLOAT, &mLODMax, "step=0.1 min=-10.0 max=10.0");
    TwAddVarRW(tweakbar(), "Scale", TW_TYPE_FLOAT, &mTextureScale, "step=0.1 min=-10.0 max=10.0");
    TwAddVarRW(tweakbar(), "Offset", TW_TYPE_FLOAT, &mTextureOffset, "step=0.1 min=-10.0 max=10.0");

    TwEnumVal magFilterEV[] = {
        {GL_NEAREST, "Nearest"}, //
        {GL_LINEAR, "Linear"},   //
    };
    TwEnumVal minFilterEV[] = {
        {GL_NEAREST, "Nearest"},                            //
        {GL_LINEAR, "Linear"},                              //
        {GL_NEAREST_MIPMAP_NEAREST, "Nearest Mip Nearest"}, //
        {GL_NEAREST_MIPMAP_LINEAR, "Nearest Mip Linear"},   //
        {GL_LINEAR_MIPMAP_NEAREST, "Linear Mip Nearest"},   //
        {GL_LINEAR_MIPMAP_LINEAR, "Linear Mip Linear"},     //
    };
    TwEnumVal wrapEV[] = {
        {GL_CLAMP_TO_EDGE, "Clamp to Edge"},               //
        {GL_REPEAT, "Repeat"},                             //
        {GL_MIRRORED_REPEAT, "Mirrored Repeat"},           //
        {GL_MIRROR_CLAMP_TO_EDGE, "Mirror Clamp to Edge"}, //
    };
    auto magFilterType = TwDefineEnum("Mag Filter", magFilterEV, 2);
    auto minFilterType = TwDefineEnum("Min Filter", minFilterEV, 6);
    auto wrapType = TwDefineEnum("Wrap", wrapEV, 4);

    TwAddVarRW(tweakbar(), "Min Filter", minFilterType, &mMinFilter, "");
    TwAddVarRW(tweakbar(), "Mag Filter", magFilterType, &mMagFilter, "");
    TwAddVarRW(tweakbar(), "Wrap", wrapType, &mWrap, "");
}

void TextureParametersSample::render(float elapsedSeconds)
{
    mCurrentTexture = glm::clamp(mCurrentTexture, 0, (int)mTextures.size() - 1);

    // set parameters
    auto tex = mTextures[mCurrentTexture];
    {
        auto t = tex->bind();
        // TODO
        t.setAnisotropicFiltering(mAnsitropicFiltering);
        t.setFilter(mMagFilter, mMinFilter);
        t.setWrap(mWrap, mWrap);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, mLODBias);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, mLODMin);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, mLODMax);
    }

    if(mAnimate)
    mRuntime += elapsedSeconds;

    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0.00, 0.33, 0.66, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", mRuntime);
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uModel", glm::rotate(mRuntime, glm::vec3{0, 1, 0}));
    shader.setTexture("uTexture", tex);
    shader.setUniform("uTexScale", mTextureScale);
    shader.setUniform("uTexOffset", mTextureOffset);

    mCube->bind().draw();
}
