uniform sampler2D uTexture;
uniform float uTexScale;
uniform float uTexOffset;

in vec2 vTexCoord;

out vec3 fColor;

void main() 
{
    fColor = texture(uTexture, vTexCoord * uTexScale + uTexOffset).rgb;
}
