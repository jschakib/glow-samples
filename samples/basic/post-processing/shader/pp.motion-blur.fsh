uniform sampler2DRect uTexture;

uniform sampler2DRect uTexPosition;
uniform sampler2DRect uTexVelocity;

uniform float uElapsedSeconds;
uniform float uMotionBlurLength;

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uPrevView;
uniform mat4 uPrevProj;

out vec4 fColor;

void main() 
{
    vec3 pos = texelFetch(uTexPosition, ivec2(gl_FragCoord.xy)).xyz;
    vec3 vel = texelFetch(uTexVelocity, ivec2(gl_FragCoord.xy)).xyz;

    vec4 spCurr = uProj * uView * vec4(pos, 1.0);
    vec4 spPrev = uPrevProj * uPrevView * vec4(pos - vel * uElapsedSeconds, 1.0);

    spCurr /= spCurr.w;
    spPrev /= spPrev.w;

    spCurr.xy = spCurr.xy * 0.5 + 0.5;
    spPrev.xy = spPrev.xy * 0.5 + 0.5;

    vec2 texSize = textureSize(uTexture);

    int steps = int(ceil(distance(spCurr.xy * texSize, spPrev.xy * texSize))) + 1;
    steps = clamp(steps, 2, 32);

    vec2 dir = spCurr.xy - spPrev.xy;
    dir *= uMotionBlurLength / uElapsedSeconds;

    vec4 color = vec4(0);
    for (int i = 0; i < steps; ++i) {
        vec2 pos = spCurr.xy + dir * (i / (steps - 1.0f) - 0.5);
        color += texture(uTexture, pos * texSize) / steps;
    }
    fColor = color;
}
