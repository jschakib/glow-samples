uniform sampler2DRect uTexture;

out vec4 fColor;

void main() 
{
    vec4 c00 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(0, 0));
    vec4 c01 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(0, 1));
    vec4 c02 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(0, 2));

    vec4 c10 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(1, 0));
    vec4 c11 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(1, 1));
    vec4 c12 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(1, 2));

    vec4 c20 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(2, 0));
    vec4 c21 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(2, 1));
    vec4 c22 = texelFetch(uTexture, ivec2(gl_FragCoord.xy - 1) + ivec2(2, 2));

    vec4 gx = c00 + 2 * c10 + c20 - (c02 + 2 * c12 + c22);
    vec4 gy = c00 + 2 * c01 + c02 - (c20 + 2 * c21 + c22);

    vec4 g = sqrt(gx * gx + gy * gy);

    fColor = g;
}
