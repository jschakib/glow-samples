in vec3 aPosition;
in vec3 aNormal;

out vec3 vPosition;
out vec3 vPrevPosition;
out vec3 vNormal;

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uModel;
uniform mat4 uPrevModel;

uniform float uOutlineStrength;
uniform bool uOutline;

void main() 
{
    vPosition = vec3(uModel * vec4(aPosition, 1.0));
    vPrevPosition = vec3(uPrevModel * vec4(aPosition, 1.0));
    vNormal = mat3(uModel) * aNormal;

    if (uOutline)
        vPosition += vNormal * uOutlineStrength;

    gl_Position = uProj * uView * vec4(vPosition, 1.0);
}
