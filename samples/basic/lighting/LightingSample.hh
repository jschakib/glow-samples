#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

enum class LightSource
{
    Point,
    Directional,
    Sphere,
};

enum class ShadingDiffuse
{
    None,
    Lambert,
    OrenNayar
};
enum class ShadingSpecular
{
    None,
    Phong,
    BlinnPhong,
    CookTorrance,
    WardAnisotropic,
    GGX
};

class LightingSample : public glow::glfw::GlfwApp
{
public:
    LightingSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedProgram mShaderBG;
    glow::SharedVertexArray mSphere;
    glow::SharedVertexArray mPlane;
    glow::SharedVertexArray mQuad;
    glow::SharedTextureCubeMap mCubeMap;

    ShadingDiffuse mShadingDiffuse = ShadingDiffuse::Lambert;
    ShadingSpecular mShadingSpecular = ShadingSpecular::None;
    glm::vec3 mDiffuse = glm::vec3(0.5f);
    glm::vec3 mSpecular = glm::vec3(1.0f);
    float mShininess = 32.0f;
    float mRoughness = 0.2f;
    float mAnisoX = 1.0f;
    float mAnisoY = 1.0f;
    bool mSpotLight = false;
    float mCubeLOD = 0.0f;

    float mReflectivity = 0.0f;

    LightSource mLightSource = LightSource::Point;
    glm::vec3 mLightDir = normalize(glm::vec3(-1, 5, 2));
    glm::vec3 mLightTarget = glm::vec3(0, 0, 0);
    glm::vec3 mAmbient = glm::vec3(0.02f);
    glm::vec3 mLightColor = {1, 1, 1};
    float mLightDis = 3.0f;
    float mLightRadius = 1.0f;
    float mLightInnerAngle = 30.0f;
    float mLightOuterAngle = 40.0f;

    float mAttenuationConstant = 1.0f;
    float mAttenuationLinear = 0.0f;
    float mAttenuationQuadratic = 0.0f;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
