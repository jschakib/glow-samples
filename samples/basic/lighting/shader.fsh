uniform vec3 uCamPos;
uniform samplerCube uCubeMap;

uniform int uShadingDiffuse;
uniform int uShadingSpecular;
uniform vec3 uDiffuse;
uniform vec3 uSpecular;
uniform float uShininess;
uniform float uReflectivity;
uniform float uRoughness;
uniform float uAnisoX;
uniform float uAnisoY;
uniform float uCubeLOD;

uniform int uLightSource;
uniform bool uSpotLight;
uniform vec3 uAmbientLight;
uniform vec3 uLightDir;
uniform vec3 uLightTarget;
uniform vec3 uLightColor;
uniform vec3 uLightPos;
uniform float uLightRadius;
uniform float uLightInnerCos;
uniform float uLightOuterCos;
uniform float uAttenuationConstant;
uniform float uAttenuationLinear;
uniform float uAttenuationQuadratic;

in vec2 vTexCoord;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vWorldPos;

out vec3 fColor;

// cook-torrance and oren nayar from https://github.com/glslify

vec3 shadingDiffuseOrenNayar(vec3 N, vec3 V, vec3 L, float roughness, vec3 albedo) 
{
    float dotVL = dot(L, V);
    float dotLN = dot(L, N);
    float dotNV = dot(N, V);

    float s = dotVL - dotLN * dotNV;
    float t = mix(1.0, max(dotLN, dotNV), step(0.0, s));

    float sigma2 = roughness * roughness;
    vec3 A = 1.0 + sigma2 * (albedo / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));    
    float B = 0.45 * sigma2 / (sigma2 + 0.09);

    return albedo * max(0.0, dotLN) * (A + B * s / t);
}

float beckmannDistribution(float x, float roughness) 
{
    float dotHN = max(x, 0.0001);
    float cos2Alpha = dotHN * dotHN;
    float tan2Alpha = (cos2Alpha - 1.0) / cos2Alpha;
    float roughness2 = roughness * roughness;
    float denom = 3.141592653589793 * roughness2 * cos2Alpha * cos2Alpha;
    return exp(tan2Alpha / roughness2) / denom;
}

vec3 shadingSpecularCookTorrance(vec3 N, vec3 V, vec3 L, float roughness, vec3 F0) 
{
    vec3 H = normalize(L + V);

    float dotVN = max(dot(V, N), 0.0);
    float dotLN = max(dot(L, N), 0.0);
    float dotLH = max(dot(L, H), 0.0);

    // G
    float dotNH = max(dot(N, H), 0.0);
    float dotVH = max(dot(V, H), 0.000001);
    float x = 2.0 * dotNH / dotVH;
    float G = min(1.0, min(x * dotVN, x * dotLN));

    // D
    float D = beckmannDistribution(dotNH, roughness);

    // F
    // vec3 F = pow(vec3(1.0 - dotVN), F0);
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5); // manually?
    vec3 F = mix(vec3(F_b), vec3(F_a), F0);

    return  G * F * D / max(3.141592653589793 * dotVN * dotLN, 0.000001);
}

vec3 shadingSpecularWardAniso(vec3 N, vec3 V, vec3 L, vec3 T, vec3 specular) 
{
    vec3 H = normalize(L + V);
    vec3 B = normalize(cross(N, T));

    float dotVN = max(dot(V, N), 0.0);
    float dotLN = max(dot(L, N), 0.0);

    if (dotVN <= 0.0 || dotLN <= 0.0)
        return vec3(0.0);

    float denom = sqrt(dotVN * dotLN) * 4 * uAnisoX * uAnisoY;
    vec3 nom = dotLN * specular;

    float ax = dot(H, T) / uAnisoX;
    float ay = dot(H, B) / uAnisoY;
    float exponent = -2 * (ax*ax + ay*ay) / (1 + dot(H, N));

    return nom / denom * exp(exponent);
}

// DO NOT MULTIPLY BY COS THETA
vec3 shadingSpecularGGX(vec3 N, vec3 V, vec3 L, float roughness, vec3 F0)
{
    // see http://www.filmicworlds.com/2014/04/21/optimizing-ggx-shaders-with-dotlh/
    vec3 H = normalize(V + L);

    float dotLH = max(dot(L, H), 0.0);
    float dotNH = max(dot(N, H), 0.0);
    float dotNL = max(dot(N, L), 0.0);
    float dotNV = max(dot(N, V), 0.0);

    float alpha = roughness * roughness;

    // D (GGX normal distribution)
    float alphaSqr = alpha * alpha;
    float denom = dotNH * dotNH * (alphaSqr - 1.0) + 1.0;
    float D = alphaSqr / (denom * denom);
    // no pi because BRDF -> lighting

    // F (Fresnel term)
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5);
    vec3 F = mix(vec3(F_b), vec3(F_a), F0);

    // G (remapped hotness, see Unreal Shading)
    float k = (alpha + 2 * roughness + 1) / 8.0;
    float G = dotNL / (mix(dotNL, 1, k) * mix(dotNV, 1, k));
    // '* dotNV' - canceled by normalization

    // '/ dotLN' - canceled by lambert
    // '/ dotNV' - canceled by G
    return D * F * G / 4.0;
}

void main() 
{
    // calculate dirs
    vec3 N = normalize(vNormal);
    vec3 T = normalize(vTangent);
    vec3 V = normalize(uCamPos - vWorldPos);
    vec3 R = reflect(-V, N);

    // anisotropic material
    if (uShadingSpecular == 4)
    {
        vec2 dir = normalize(vWorldPos.xz);
        T = vec3(
            -dir.y,
            0,
            dir.x
        );
    }

    // calculate light source
    vec3 L = normalize(uLightPos - vWorldPos);
    vec3 lightColor = uLightColor;
    vec3 lightPos = uLightPos;
    if (uLightSource == 1) // directional light
    {
        L = uLightDir;
    }
    if (uSpotLight) // spot light
    {
        vec3 spotDir = normalize(lightPos - uLightTarget);
        float dotSL = dot(spotDir, L);
        lightColor *= 1 - smoothstep(uLightInnerCos, uLightOuterCos, dotSL);
    }
    if (uLightSource == 2) // sphere area light
    {
        L = lightPos - vWorldPos;
        float origLdis = length(L);
        vec3 c2ray = dot(L, R) * R - L;
        lightPos += c2ray * clamp(uLightRadius / length(c2ray), 0.0, 1.0);
        L = normalize(lightPos - vWorldPos);

        float a = sqrt(max(uRoughness, 0.001));
        float a2 = min(1.0, a + uLightRadius / (2 * origLdis));
        float areaNormalization = pow(a / a2, 2);
        lightColor *= areaNormalization;
    }

    // attenuation
    if (uLightSource != 1) // for non-dir only
    {
        float dis = distance(lightPos, vWorldPos);
        lightColor *= 1 / (uAttenuationConstant + uAttenuationLinear * dis + uAttenuationQuadratic * dis * dis);
    }

    // ray-traced shadow
    {
        vec3 sphereCenter = vec3(0,0,0);
        float sphereRadius = 1.0;
        vec3 pos = vWorldPos;
        vec3 dir = uLightSource == 1 ? uLightDir : normalize(uLightPos - vWorldPos);
        vec3 toSphere = sphereCenter - pos;
        float t = dot(toSphere, dir);
        float dis = distance(pos + dir * t, sphereCenter);
        float lightSize = uLightRadius * abs(t) / distance(uLightPos, vWorldPos);
        if (uLightSource != 2)
            lightSize = 0.001;
        if (t > 0 && dis < sphereRadius + lightSize)
            lightColor *= smoothstep(sphereRadius - lightSize, sphereRadius + lightSize, dis); // in shadow
    }

    // precalc dots
    vec3 H = normalize(V + L);
    float dotNH = dot(N, H);
    float dotNL = dot(N, L);
    float dotRL = dot(R, L);

    // calc color
    vec3 color = vec3(0.0);
    {
        // ambient
        color += uAmbientLight * uDiffuse;

        // diffuse
        {
            // lambert
            if (uShadingDiffuse == 1)
            {
                color += lightColor * uDiffuse * max(0.0, dotNL);
            }

            // oren-nayar
            if (uShadingDiffuse == 2)
            {
                color += lightColor * shadingDiffuseOrenNayar(N, V, L, uRoughness, uDiffuse);
            }
        }

        // specular
        {
            // phong
            if (uShadingSpecular == 1 && dotNL > 0)
            {
                color += lightColor * uSpecular * pow(max(0.0, dotRL), uShininess);
            }

            // blinn-phong
            if (uShadingSpecular == 2 && dotNH > 0)
            {
                color += lightColor * uSpecular * pow(max(0.0, dotNH), uShininess);
            }

            // cook-torrance
            if (uShadingSpecular == 3)
            {
                color += lightColor * shadingSpecularCookTorrance(N, V, L, max(0.01, uRoughness), uSpecular);
            }

            // ward anisotropic
            if (uShadingSpecular == 4)
            {
                color += lightColor * shadingSpecularWardAniso(N, V, L, T, uSpecular);
            }

            // ggx
            if (uShadingSpecular == 5)
            {
                color += lightColor * shadingSpecularGGX(N, V, L, max(0.01, uRoughness), uSpecular);
            }
        }

        // skybox reflection
        color += uReflectivity * textureLod(uCubeMap, R, uCubeLOD).rgb;
    }

    // gamma correction
    fColor = pow(color, vec3(1/2.2));
}
