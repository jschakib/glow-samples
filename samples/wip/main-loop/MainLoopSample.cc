#include "MainLoopSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void MainLoopSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mMainLoop = geometry::Cube<>().generate();

    auto cam = getCamera();
    cam->setLookAt({2, 5, 4}, {0, 0, 0});

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");
    TwAddVarRW(tweakbar(), "Update Rate (Hz)", TW_TYPE_DOUBLE, &mUpdateRate, "min=1");
    TwAddVarRW(tweakbar(), "VSync", TW_TYPE_BOOLCPP, &mVSync, "");
}

void MainLoopSample::render(float elapsedSeconds)
{
    // Set refresh rate and vsync (changes are applied for next frame)
    setUpdateRate(mUpdateRate);
    setVSync(mVSync);

    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", mRuntime);
    shader.setUniform("uView", cam->getViewMatrix());
    shader.setUniform("uProj", cam->getProjectionMatrix());
    shader.setUniform("uModel", glm::rotate(mRuntime, glm::vec3{0, 1, 0}));

    mMainLoop->bind().draw();
}

void MainLoopSample::update(float elapsedSeconds)
{
    if (mAnimate)
        mRuntime += elapsedSeconds;
}
