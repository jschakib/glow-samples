in vec3 aPosition;
in vec3 aNormal;
in vec3 aTangent;

out vec3 vsNormal;
out vec3 vsTangent;

void main() {
    gl_Position = vec4(aPosition, 1);
    vsNormal = aNormal;
    vsTangent = aTangent;
}
