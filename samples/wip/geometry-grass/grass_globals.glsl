layout (std140) uniform uGrassUBO
{
    vec4 packedWorldBounds;

    vec3 cameraUp;
    float cameraUpBias;

    float bladeThickness;
    float bladeLength;
    float windSpeed;
    float windStrength;

    mat4 vp;
    mat4 cleanVp;
    mat4 prevCleanVp;

    vec2 inverseTrampleTexSize;
    float runtime;
} uGrassInfo;

const vec3 bladeBottomColor = vec3(0, .125, .075);
const vec3 bladeTopColorA = vec3(.5, 1, .4);
const vec3 bladeTopColorB = vec3(.3, .8, .2);

vec2 getFragmentVelocity(in vec4 cleanHDC, in vec4 prevCleanHDC)
{
    vec2 a = (cleanHDC.xy / cleanHDC.w) * 0.5 + 0.5;
    vec2 b = (prevCleanHDC.xy / prevCleanHDC.w) * 0.5 + 0.5;
    return a - b;
}

vec2 getTrampleUv(in vec2 pos)
{
    return vec2((pos.x - uGrassInfo.packedWorldBounds.x) / uGrassInfo.packedWorldBounds.y, (pos.y - uGrassInfo.packedWorldBounds.z) / uGrassInfo.packedWorldBounds.w);
}

vec2 getWorldPosition(in ivec2 computeInvocation)
{
    return vec2(
                uGrassInfo.packedWorldBounds.x + (computeInvocation.x * uGrassInfo.inverseTrampleTexSize.x) * uGrassInfo.packedWorldBounds.y,
                uGrassInfo.packedWorldBounds.z + (computeInvocation.y * uGrassInfo.inverseTrampleTexSize.y) * uGrassInfo.packedWorldBounds.w
                );
}

float length2(in vec2 val)
{
    return dot(val, val);
}
