cmake_minimum_required(VERSION 3.0)
project(PathTracer)

file(GLOB_RECURSE FILES "*.cc" "*.hh" "*.*sh")

add_executable(${PROJECT_NAME} ${FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC glow glow-extras glfw AntTweakBar)
target_compile_options(${PROJECT_NAME} PUBLIC ${GLOW_SAMPLES_DEF_OPTIONS})
set_property(TARGET ${PROJECT_NAME} PROPERTY FOLDER "Samples/WIP")
