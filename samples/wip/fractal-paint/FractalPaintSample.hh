#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class FractalPaintSample : public glow::glfw::GlfwApp
{
public:
    FractalPaintSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShaderFractal;
    glow::SharedProgram mShaderLine;
    glow::SharedVertexArray mQuad;
    glow::SharedFramebuffer mFramebufferCanvas;

    glow::SharedTexture2D mCanvas;

    int mWidth = 2;
    int mHeight = 2;

    float mHueShift = 0;
    float mSaturationShift = 0;
    float mValueShift = 0;
    int mFractalDepth = 1;

    float mPixelWidth = 3;
    // float mAlpha = 1;
    glm::vec4 mColor = {0, 1, 0, 1};

    bool mDepthTop = true;

    bool mDrawing = false;
    glm::vec2 mLastDrawPos = {-1, -1};

    std::vector<std::vector<glm::vec2>> mConnectors;
    glm::vec2 mBaseStart = {.3, .2};
    glm::vec2 mBaseEnd = {.7, .2};

    std::vector<std::pair<glm::vec2, glm::vec2>> mDrawLines;

    void drawLine(glm::vec2 from, glm::vec2 to, float pxWidth, glm::vec4 color);

protected:
    void init() override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;
    bool onMousePosition(double x, double y) override;
    bool onMouseButton(double x, double y, int button, int action, int mods, int clickCount) override;
};
