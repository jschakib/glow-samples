uniform sampler2D uTexture;

uniform vec3 mHsvShift;

in vec2 vTexCoord;

out vec4 fColor;


vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
    vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    fColor = texture(uTexture, vTexCoord);
    //fColor = vec4(1,0,0,1);
    vec3 hsv = rgb2hsv(fColor.rgb);
    hsv.gb += mHsvShift.gb / 100.0;
    hsv.r += mHsvShift.r / 360.0;
    hsv.gb = clamp(hsv.gb, vec2(0), vec2(1));
    fColor.rgb = hsv2rgb(hsv);
}
