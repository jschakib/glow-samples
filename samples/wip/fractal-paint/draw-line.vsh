in vec2 aPosition;

uniform mat4 uModel;

void main() {
    vec4 pos = uModel[gl_VertexID];
    gl_Position = vec4(pos.x * 2 - 1, pos.y * 2 - 1, 0, 1);
}
