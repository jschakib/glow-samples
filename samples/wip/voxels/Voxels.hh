#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>
#include <glow/fwd.hh>
#include <glow/std140.hh>

class VoxelSample;

enum class Direction
{
    PosX,
    NegX,
    PosY,
    NegY,
    PosZ,
    NegZ
};

struct VoxelData
{
    int mat;
    glm::vec4 color;
};

struct VoxelVertex
{
    glm::vec3 pos;
    glm::vec2 uv;
    int idx;
};

struct LightData
{
    // total light emitted (diffusely) by contained faces (is premultiplied)
    // 0 for 0 opacity
    glm::vec3 emission;
    float opacity;
};

struct GpuVoxel
{
    glow::std140vec3 normal;
    glow::std140mat4 colors;
    glow::std140mat4 lights;
};

class Voxels
{
public:
    static constexpr int Size = 64;
    static constexpr int StepX = 1;
    static constexpr int StepY = Size;
    static constexpr int StepZ = Size * Size;

    // each voxel from (x,y,z) to (x+1,y+1,z+1)
    std::vector<VoxelData> data;
    // each entry maps to the light passing through one quad of the voxel
    // e.g. PosY for (0,0,0) is the quad from (0,1,0) to (1,1,1)
    std::vector<std::vector<LightData>> lightmap[6];

    // backref to app
    VoxelSample* sample = nullptr;

    Voxels(VoxelSample* sample);

    bool contains(glm::ivec3 p) const { return contains(p.x, p.y, p.z); }
    bool contains(int x, int y, int z) const
    {
        if (x < 0 || x >= Size)
            return false;
        if (y < 0 || y >= Size)
            return false;
        if (z < 0 || z >= Size)
            return false;
        return true;
    }

    bool isBoundary(int x, int y, int z) const
    {
        if (x == 0 || x == Size - 1)
            return true;
        if (y == 0 || y == Size - 1)
            return true;
        if (z == 0 || z == Size - 1)
            return true;
        return false;
    }
    int idxOf(int x, int y, int z) const { return (z * Size + y) * Size + x; }
    int lightIdxOf(int x, int y, int z, int lvl) const { return (z * (Size >> lvl) + y) * (Size >> lvl) + x; }
    int lightStepX(int lvl) const { return 1; }
    int lightStepY(int lvl) const { return Size >> lvl; }
    int lightStepZ(int lvl) const { return (Size >> lvl) * (Size >> lvl); }
    void generate();

    glm::ivec3 idxToPos(int idx) const
    {
        auto x = idx % Size;
        idx /= Size;
        auto y = idx % Size;
        idx /= Size;
        auto z = idx;
        return {x, y, z};
    }
    glm::ivec3 stepToDir(int step) const
    {
        if (step == StepX)
            return {1, 0, 0};
        if (step == -StepX)
            return {-1, 0, 0};
        if (step == StepY)
            return {0, 1, 0};
        if (step == -StepY)
            return {0, -1, 0};
        if (step == StepZ)
            return {0, 0, 1};
        if (step == -StepZ)
            return {0, 0, -1};
        assert(0 && "invalid step");
        return {0, 0, 0};
    }

    void updateLighting();

    /// (x,y,z) is lower corner
    glm::vec3 lightingAt(int idx, int sdx, int sdy, int sn, Direction dir) const;

    glm::vec3 traceCone(Direction ldir, glm::ivec3 pos, glm::ivec3 dir, glm::ivec3 dx, glm::ivec3 dy, int debug) const;

    glow::SharedVertexArray buildMesh(glow::SharedShaderStorageBuffer const& voxelData) const;
};
