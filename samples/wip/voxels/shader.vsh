in vec3 aPosition;
in vec2 aUV;
in int aIdx;

out vec3 vPosition;
out vec2 vUV;
flat out int vIdx;

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uModel;

void main() {
    vPosition = aPosition;
    vUV = aUV;
    vIdx = aIdx;
    
    gl_Position = uProj * uView * uModel * vec4(aPosition, 1.0);
}
