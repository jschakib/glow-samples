#define UDISTANCE_MAX 0x6fffffff

layout(std430, binding = 0) readonly restrict buffer bPoints
{
    vec4 points[];
};

uniform layout(binding=0, r32ui) restrict coherent uimage3D uTexDistanceField;
uniform layout(binding=1, r32ui) restrict uimage3D uTexNearestPoint;

uniform float uMaxDistance;

uint dis2udis(float dis)
{
    return uint(dis / uMaxDistance * UDISTANCE_MAX);
}

bool updateDistance(ivec3 ip, uint udis, uint ptIdx)
{
    uint prevDis = imageAtomicMin(uTexDistanceField, ip, udis);
    //uint prevDis = imageLoad(uTexDistanceField, ip).r;
    
    if (udis < prevDis)
    {
        memoryBarrierImage();

        uint checkDis = imageLoad(uTexDistanceField, ip).r;

        if (checkDis == udis)
        {
            // is already same: imageStore(uTexDistanceField, ip, uvec4(udis));
            imageStore(uTexNearestPoint, ip, uvec4(ptIdx));

            return true;
        }
    }

    return false;
}
