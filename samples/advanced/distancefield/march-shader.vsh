#version 430 core

in uint aPosition;
out uint vPosition;

void main() {
    vPosition = aPosition;
}
