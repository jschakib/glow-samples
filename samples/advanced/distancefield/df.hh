#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <array>

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>

namespace df
{
template <typename PointT = glm::vec2, typename IndexT = size_t>
struct Grid2D
{
    IndexT width;
    IndexT height;

    IndexT size;

    Grid2D(IndexT w, IndexT h) : width(w), height(h), size(w * h) {}

    PointT pointAt(IndexT idx) const { return PointT(idx % width, idx / width); }
    IndexT idxOf(PointT const& pt) const
    {
        // IndexT x = glm::clamp((int32_t)glm::round(pt.x), 0, width - 1);
        // IndexT y = glm::clamp((int32_t)glm::round(pt.y), 0, height - 1);
        // return y * width + x;
		return -1;
    }
};

template <typename PointT = glm::vec3, typename IndexT = size_t>
struct Grid3D
{
    IndexT width;
    IndexT height;
    IndexT depth;

    IndexT size;

    Grid3D(IndexT w, IndexT h, IndexT d) : width(w), height(h), depth(d), size(w * h * d) {}

    PointT pointAt(IndexT idx) const { return PointT(idx % width, (idx / width) % height, idx / (width * height)); }
    IndexT idxOf(PointT const& pt) const
    {
        IndexT x = glm::clamp(glm::round(pt.x), 0.0f, width - 1.0f);
        IndexT y = glm::clamp(glm::round(pt.y), 0.0f, height - 1.0f);
        IndexT z = glm::clamp(glm::round(pt.z), 0.0f, depth - 1.0f);
        return (z * height + y) * width + x;
    }

    std::array<IndexT, 6> neighbors(IndexT idx) const
    {
        auto x = idx % width;
        auto y = (idx / width) % height;
        auto z = idx / (width * height);
        auto invalid = size;
        auto dx = 1;
        auto dy = width;
        auto dz = width * height;
        return {
            x > 0 ? idx - dx : invalid,         //
            x < width - 1 ? idx + dx : invalid, //

            y > 0 ? idx - dy : invalid,          //
            y < height - 1 ? idx + dy : invalid, //

            z > 0 ? idx - dz : invalid,         //
            z < depth - 1 ? idx + dz : invalid, //
        };
    }
};

struct GlmVec3Config
{
    using PrimitiveT = glm::vec4;
    using PointT = glm::vec3;
    using IndexT = uint32_t;
    using DistanceT = float;
    using GridT = Grid3D<glm::vec3, size_t>;

    static DistanceT distance(PrimitiveT const& prim, PointT const& point) { return glm::distance((glm::vec3)prim, point); }
    static bool is_prim1_closer(PrimitiveT const& prim1, PrimitiveT const& prim2, PointT const& point)
    {
        return glm::distance2((glm::vec3)prim1, point) < glm::distance2((glm::vec3)prim2, point);
    }
    static PointT pointOf(PrimitiveT const& p) { return p; }
};

template <typename ConfigT>
void nearest_field(std::vector<typename ConfigT::PrimitiveT> const& primitives,
                   std::vector<typename ConfigT::IndexT>& nearest,
                   typename ConfigT::GridT const& grid)
{
	size_t cellsTouched = 0;
    auto size = grid.size;
    auto prims = primitives.size();
    nearest.resize(size);
    assert(prims > 0 && "no primitives found");

    // clear
    // #pragma omp parallel for
    for (size_t i = 0; i < size; ++i)
        nearest[i] = 0; // point to first primitive

    // init
    std::vector<size_t> queue;
    for (size_t pi = 1; pi < prims; ++pi) // 1st prim is already filled
    {
        // get cell of primitive
        auto const& p1 = primitives[pi];
        auto gi = grid.idxOf(ConfigT::pointOf(p1));
        assert(0 <= gi && gi < grid.size && "out of range");

        // get previous primitive
        auto pi2 = nearest[gi];
        auto const& p2 = primitives[pi2];

        // check if new one is closer
        auto p = grid.pointAt(gi);
        if (ConfigT::is_prim1_closer(p1, p2, p))
        {
            // update nearest
            nearest[gi] = pi;

            // add to queue if first "touch"
            if (pi2 == 0)
                queue.push_back(gi);
        }
    }
    std::cout << "CPU Seeds: " << queue.size() << std::endl;

    // iterate
    std::vector<size_t> newQueue;
    while (!queue.empty())
    {
        // clear new queue
		cellsTouched += queue.size();
        newQueue.clear();

        // expand queue elements
		auto qSize = queue.size();
		for (size_t qi = 0; qi < qSize; ++qi)
        {
			auto gi = queue[qi];
			auto pi1 = nearest[gi];
            auto const& p1 = primitives[pi1];

            // check neighbors
            for (auto ni : grid.neighbors(gi))
                if (ni != size) // invalid neighbor
                {
					auto const& p2 = primitives[nearest[ni]];
					auto p = grid.pointAt(ni);

					// if closer, update
					if (ConfigT::is_prim1_closer(p1, p2, p))
					{
						nearest[ni] = pi1;

						// and enqueue neighbor
						newQueue.push_back(ni);
					}
                }
        }
		std::cout << "  queue: " << newQueue.size() << std::endl;

        // switch queues
        using std::swap;
        swap(queue, newQueue);
    }

	std::cout << " - touched " << cellsTouched << " cells (" << (cellsTouched * 100.0f / size) << "%)" << std::endl;
}

template <typename ConfigT>
void to_distance_field(std::vector<typename ConfigT::PrimitiveT> const& primitives,
                       std::vector<typename ConfigT::IndexT> const& nearest_field,
                       std::vector<typename ConfigT::DistanceT>& distance_field,
                       typename ConfigT::GridT const& grid)
{
    auto size = grid.size();
    assert(size == nearest_field.size() && "nearest_field has wrong size");

    distance_field.resize(size);
    // #pragma omp parallel for
    for (size_t i = 0; i < size; ++i)
        distance_field[i] = ConfigT::distance(primitives[nearest_field[i]], grid.pointAt(i));
}
}
