#version 430 core

in vec4 aPosition;
out vec4 vPosition;
out int vPointID;

void main() {
    vPosition = aPosition;
    vPointID = gl_VertexID;
}
