#pragma once

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include <glm/glm.hpp>

class PythagorasTreeSample : public glow::glfw::GlfwApp
{
public:
    PythagorasTreeSample() : GlfwApp(Gui::AntTweakBar) {}

private:

    const int mMaxDepth = 6;
    const float mHeightFactor = 1.5f;
    const float mMidFactor = 1.3f;
    const float mNearFactor = 0.8f;

    const glm::vec3 mVertex0 = {0, 0, 0};
    const glm::vec3 mVertex1 = {1, 0, 0};
    const glm::vec3 mVertex2 = {0, 0, 1};

    float mRuntime = 0.0f;

    glow::SharedProgram mShader;
    glow::SharedVertexArray mTree;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
