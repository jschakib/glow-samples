uniform float uRuntime = 0.0;

vec3 uColor0 = vec3(.545, .271, .075);
vec3 uColor1 = vec3(0, .502, 0.);

uniform mat4 uView;
uniform mat4 uProj;
uniform mat4 uModel;
