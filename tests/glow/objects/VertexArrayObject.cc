#include <doctest.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/util/LocationMapping.hh>

using namespace glow;

TEST_CASE("VertexArray, Binding")
{
    auto vao0 = VertexArray::create();
    CHECK(VertexArray::getCurrentVAO() == nullptr);

    {
        auto bvao0 = vao0->bind();
        CHECK(VertexArray::getCurrentVAO() == &bvao0);

        auto vao1 = VertexArray::create();
        auto vao2 = VertexArray::create();
        CHECK(VertexArray::getCurrentVAO() == &bvao0);

        {
            auto bvao1 = vao1->bind();
            CHECK(VertexArray::getCurrentVAO() == &bvao1);

            auto bvao2 = vao2->bind();
            CHECK(VertexArray::getCurrentVAO() == &bvao2);
        }

        CHECK(VertexArray::getCurrentVAO() == &bvao0);
    }

    CHECK(VertexArray::getCurrentVAO() == nullptr);
}

TEST_CASE("VertexArray, Locations")
{
    auto ab0 = ArrayBuffer::create();
    ab0->defineAttribute<float>("A");
    ab0->defineAttribute<float>("B");
    ab0->bind().setData({glm::vec2(0, 0)});

    auto ab1 = ArrayBuffer::create();
    ab1->defineAttribute<float>("D");
    ab1->defineAttribute<float>("C");
    ab1->bind().setData({glm::vec2(0, 0)});

    auto ab2 = ArrayBuffer::create();
    ab2->defineAttribute<float>("E");
    ab2->bind().setData({0.0f});

    auto vs0 = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                          "in float E;\n"
                                                          "in float B;\n"
                                                          "in float C;\n"
                                                          "in float A;\n"
                                                          "in float D;\n"
                                                          "out float x;\n"
                                                          "void main() { x = A + B + C + D + E; }");

    auto vs1 = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                          "in float E;\n"
                                                          "in float A;\n"
                                                          "in float D;\n"
                                                          "in float C;\n"
                                                          "out float x;\n"
                                                          "void main() { x = A + C + D + E; }");

    auto fs = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 430 core\n"
                                                           "in float x;\n"
                                                           "out float y;\n"
                                                           "out float z;\n"
                                                           "void main() { y = x; z = x / 2; }");

    auto prog0 = Program::create({vs0, fs});
    auto prog1 = Program::create({vs1, fs});

    auto vao0 = VertexArray::create({ab0, ab1, ab2});
    auto vao1 = VertexArray::create({ab2, ab1, ab0});

    auto tex1 = Texture2D::create();
    auto tex2 = Texture2D::create();
    auto fbo = Framebuffer::create({{"z", tex1}, {"y", tex2}});

    CHECK(vao0->getAttributeMapping()->count() == 5);
    CHECK(vao1->getAttributeMapping()->count() == 5);
    CHECK(prog0->getAttributeMapping()->count() == 5);
    CHECK(prog1->getAttributeMapping()->count() == 4);

    CHECK(vao0->getAttributeMapping()->queryLocation("A") == 0);
    CHECK(vao0->getAttributeMapping()->queryLocation("B") == 1);
    CHECK(vao0->getAttributeMapping()->queryLocation("D") == 2);
    CHECK(vao0->getAttributeMapping()->queryLocation("C") == 3);
    CHECK(vao0->getAttributeMapping()->queryLocation("E") == 4);

    CHECK(vao1->getAttributeMapping()->queryLocation("E") == 0);
    CHECK(vao1->getAttributeMapping()->queryLocation("D") == 1);
    CHECK(vao1->getAttributeMapping()->queryLocation("C") == 2);
    CHECK(vao1->getAttributeMapping()->queryLocation("A") == 3);
    CHECK(vao1->getAttributeMapping()->queryLocation("B") == 4);

    CHECK(prog0->getAttributeMapping()->queryLocation("A") != -1);
    CHECK(prog0->getAttributeMapping()->queryLocation("B") != -1);
    CHECK(prog0->getAttributeMapping()->queryLocation("C") != -1);
    CHECK(prog0->getAttributeMapping()->queryLocation("D") != -1);
    CHECK(prog0->getAttributeMapping()->queryLocation("E") != -1);

    CHECK(prog1->getAttributeMapping()->queryLocation("A") != -1);
    CHECK(prog1->getAttributeMapping()->queryLocation("B") == -1);
    CHECK(prog1->getAttributeMapping()->queryLocation("C") != -1);
    CHECK(prog1->getAttributeMapping()->queryLocation("D") != -1);
    CHECK(prog1->getAttributeMapping()->queryLocation("E") != -1);

    CHECK(fbo->getFragmentMapping()->queryLocation("z") == 0);
    CHECK(fbo->getFragmentMapping()->queryLocation("y") == 1);

    // "draw"
    for (int i = 0; i < 10; ++i)
    {
        {
            auto p0 = prog0->use();

            vao0->bind().draw();
            CHECK(vao0->getAttributeMapping() == prog0->getAttributeMapping());
        }

        {
            auto fb = fbo->bind();

            auto p1 = prog1->use();

            vao0->bind().draw();
            vao1->bind().draw();
        }
    }

    auto refMap = vao0->getAttributeMapping();
    CHECK(refMap == vao0->getAttributeMapping());
    CHECK(refMap == vao1->getAttributeMapping());
    CHECK(refMap == prog0->getAttributeMapping());
    CHECK(refMap == prog1->getAttributeMapping());

    CHECK(prog1->getFragmentMapping() == fbo->getFragmentMapping());

    CHECK(refMap->queryLocation("A") >= 0);
    CHECK(refMap->queryLocation("B") >= 0);
    CHECK(refMap->queryLocation("C") >= 0);
    CHECK(refMap->queryLocation("D") >= 0);
    CHECK(refMap->queryLocation("E") >= 0);
}

TEST_CASE("VertexArray, LocationsFuzzy")
{
    // TODO
}
