#include <doctest.hh>

#include <fstream>

#include <glow/objects/Shader.hh>

using namespace glow;

TEST_CASE("Shader, Create")
{
    auto vs = Shader::createFromSource(GL_VERTEX_SHADER, "#version 430 core\n"
                                                   "void main() { }");
    CHECK(vs->isCompiledWithoutErrors());

    auto fs = Shader::createFromSource(GL_FRAGMENT_SHADER, "#version 430 core\n"
                                                     "void main() { }");
    CHECK(fs->isCompiledWithoutErrors());

    auto gs = Shader::createFromSource(GL_GEOMETRY_SHADER, "#version 430 core\n"
                                                     "void main() { }");
    CHECK(gs->isCompiledWithoutErrors());

    auto tcs = Shader::createFromSource(GL_TESS_CONTROL_SHADER, "#version 430 core\n"
                                                          "void main() { }");
    CHECK(tcs->isCompiledWithoutErrors());

    auto tes = Shader::createFromSource(GL_TESS_EVALUATION_SHADER, "#version 430 core\n"
                                                             "void main() { }");
    CHECK(tes->isCompiledWithoutErrors());

    auto cs = Shader::createFromSource(GL_COMPUTE_SHADER, "#version 430 core\n"
                                                    "void main() { }");
    CHECK(cs->isCompiledWithoutErrors());
}

TEST_CASE("Shader, FromFile")
{
    {
        std::ofstream("/tmp/shader.glsl") << "#version 430 core\n"
                                             "void main() { }\n";
    }

    auto vs = Shader::createFromFile(GL_VERTEX_SHADER, "/tmp/shader.glsl");
    CHECK(vs->isCompiledWithoutErrors());
}
