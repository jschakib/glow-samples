uniform uint uParticleCount;

// mat4 vs Particle: no difference
// vec4 vs Particle: 1/4th (exact mem diff)
struct Particle {
    vec3 pos;
    mat3 frame;
};

layout(std140) readonly restrict buffer bIn {
    restrict Particle particlesIn[];
};

layout(std140) writeonly restrict buffer bOut {
    restrict Particle particlesOut[];
};

uint wang_hash(uint seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

void main()
{
    uint idx = gl_GlobalInvocationID.x;

    if (idx >= uParticleCount)
        return;

    particlesOut[idx] = particlesIn[wang_hash(idx) % uParticleCount];
    //particlesOut[idx] = particlesIn[idx];
}
