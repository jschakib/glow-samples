#include <doctest.hh>

#include <glm/glm.hpp>

#include <glow-extras/geometry/Cube.hh>

TEST_CASE("ExtrasGeometry, Cube")
{
    using namespace glow;

    struct MyCubeVertex
    {
        static std::vector<ArrayBufferAttribute> attributes()
        {
            return {
                {&MyCubeVertex::pos, "aPosition"}, //
                {&MyCubeVertex::normal, "aNormal"},
            };
        }

        tg::pos3 pos;
        tg::vec3 normal;

        MyCubeVertex() = default;
        MyCubeVertex(tg::pos3 p, tg::vec3 n, tg::vec3, tg::pos2) : pos(p), normal(n) {}
    };

    auto cube0 = geometry::Cube<>().generate();
    auto cube1 = geometry::Cube<MyCubeVertex>().generate();
}
