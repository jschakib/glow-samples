#include <doctest.hh>

#include "glow/common/str_utils.hh"

using namespace glow;

TEST_CASE("glow string helper")
{
    CHECK(glow::util::endswith("blaa", "laa"));
    CHECK(!glow::util::endswith("laa", "blaa"));

    CHECK(glow::util::fileEndingOf("/path/to\\myfile.foo.png") == ".png");
    CHECK(glow::util::fileEndingOf("/path\\to/myfile") == "");
    CHECK(glow::util::fileEndingOf("/path\\to.png/myfile") == "");
    CHECK(glow::util::fileEndingOf("/path\\to/myfile.png") == ".png");
}
